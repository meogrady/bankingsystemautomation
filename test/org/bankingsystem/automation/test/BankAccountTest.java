package org.bankingsystem.automation.test;

import java.util.Date;
import org.bankingsystem.automation.BankAccount;
import org.junit.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Mark
 *
 */
public class BankAccountTest {
	
	static BankAccount bk;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println ("Begin Executing BankAccount Test...");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println ("End Executing BankAccount Test...");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		bk = new BankAccount ();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		bk = null;
	}

	@Test
	public void testBankAccountDefaultConstructor () {
		//bk = new BankAccount ();
		// test default balance
		Assert.assertEquals ("Expected balance != actual", 0.00, bk.getBalance(), 0);
		// test default account number
		Assert.assertEquals ("Failed","Z01", bk.getAccountNumber());
		// test default client name
		Assert.assertEquals ("Expected client name != actual", "Nobody", bk.getClientName());
		// test default creation date
        Assert.assertEquals(String.valueOf(new Date()), String.valueOf(bk.getAccountCreationDate()));
	}
	
	@Test
	public void testBankAccountNonDefaultConstructor () {
		bk = new BankAccount (100.00, "Z01", "Client Name");
		// test initial balance
		Assert.assertEquals ("Expected != actual", 100.00, bk.getBalance(), 0);
		// test initial account number
		Assert.assertEquals ("Expected != actual", "Z01", bk.getAccountNumber());
		// test client name
		Assert.assertEquals ("Expected != actual", "Client Name", bk.getClientName());
	}
	
	@Test
	public void testBankAccountDeposit () {
		bk = new BankAccount ();
		// test positive deposit amount
		bk.deposit(129.32);
		Assert.assertEquals ("Expected != actual", 129.32, bk.getBalance(), 0);
	}

}
