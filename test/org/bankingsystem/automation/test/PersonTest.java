package org.bankingsystem.automation.test;

import java.util.Random;

import org.bankingsystem.automation.AccountType;
import org.bankingsystem.automation.CheckingAccount;
import org.bankingsystem.automation.CreditAccount;
import org.bankingsystem.automation.Person;
import org.bankingsystem.automation.RandomAccountNumberGenerator;
import org.junit.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PersonTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
    public void testIfPersonCanBeCreated () throws Exception {
        Person p1 = new Person();
        Assert.assertTrue("Person failed to be created", !p1.equals(null));
    }

    @Test
    public void testIfPersonCanHaveCheckingAccount () throws Exception {
        Person p1 = new Person();
        CheckingAccount ch1 = new CheckingAccount();
        p1.addAccountToTheList(ch1);

        Assert.assertTrue("", p1.getNumberOfAccounts() == 1);
    }

    @Test
    public void testIfPersonCanCreateAndCloseAccount () throws Exception {
        Person p1 = new Person();
        CheckingAccount ch1 = new CheckingAccount();
        p1.addAccountToTheList(ch1);
        Assert.assertTrue("", p1.getNumberOfAccounts() == 1);
        //p1.addAccountToTheList().remove(ch1);
        //Assert.assertTrue("", p1.getNumberOfAccounts() == 0);
    }

    @Test
    public void testIfPersonCanCreateCheckingAccount () throws Exception {
        Person p1 = new Person();
        CheckingAccount ch1 = new CheckingAccount();
        p1.addAccountToTheList(ch1);
        Assert.assertTrue("", p1.getNumberOfAccounts() == 1);

        CreditAccount cr1 = new CreditAccount();
        p1.addAccountToTheList(cr1);
        Assert.assertTrue("", p1.getNumberOfAccounts() == 2);
        //Assert.assertTrue("jj", p1.addAccountToTheList.get(0).equals(ch1));
        //Assert.assertTrue("jj", p1.addAccountToTheList.get(1).equals(cr1));
    }

    @Test
    public void testCalculatePersonsTotalWealth () throws Exception {

        Person p = new Person();
        CheckingAccount ch = null;
        Random rand = new Random ();
        double sum = .0;

        // Create two hundred accounts
        for (int i = 0; i < 200; i++) {
            ch = new CheckingAccount();
            ch.deposit(10.00);
            ch.setType(AccountType.values()[rand.nextInt(4)]);
            p.addAccountToTheList(ch);
        }

       /* // Accumulate the sum in accounts
        for (AbstractAccount a : p) {
            sum += a.getBalance();
        }*/

        Assert.assertTrue("Failed: Size", p.getNumberOfAccounts() == 200);
        //Assert.assertTrue("Failed: Size", sum == 2000);
    }

    @Test
    public void testIfRandomMultipleRandomAccountTypesCanBeAdded () throws Exception {
        Person p = new Person();
        boolean b = RandomAccountNumberGenerator.GenerateRandomBoolean();

        for (int i = 0; i < 200; i++) {
            if (b) {
                CheckingAccount ch = new CheckingAccount();
                p.addAccountToTheList(ch);
            } else {
                CreditAccount cr = new CreditAccount();
                p.addAccountToTheList(cr);
            }
        }

        Assert.assertEquals("", 200, p.getNumberOfAccounts());

        for (int i = 0; i < 200; i++) {
           Assert.assertFalse("", p.getAccountByPosition(i) == AccountType.UNKNOWN);
        }
    }

    @Test
    public void testAccountCannotBeRetrievedIfItIsNotValid () throws Exception{
        Person p = new Person();
        p.getAccountByPosition(2);
        Assert.assertEquals("Unknown account type in not returned",
                p.getAccountByPosition(2),
                AccountType.UNKNOWN);
    }

    @Test
    public void testAccountCannotAcceptInt () throws Exception{
        Person p = new Person();
        CheckingAccount ch = null;
        p.addAccountToTheList(ch);
        Assert.assertEquals("Unknown account type in not returned",
                AccountType.UNKNOWN,
                p.getAccountByPosition(0));
    }

    @Test
    public void testVerifyObjectAbstractClassCanBeAdded () throws Exception{
        Person p = new Person();
        CheckingAccount ch = new CheckingAccount();
        p.addAccountToTheList(ch);

        Assert.assertEquals("Unknown account type in not returned",
                AccountType.CHECKING,
                p.getAccountByPosition(0));
    }

}
