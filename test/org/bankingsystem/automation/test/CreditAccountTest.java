package org.bankingsystem.automation.test;

import org.junit.Assert;
import org.bankingsystem.automation.CheckingAccount;
import org.bankingsystem.automation.CreditAccount;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/** Copyright (C) 2013 Mark O'Grady. All Rights Reserved.
*
*  Project:     white-box Testing with JUnit
*  Program:     CreditAccountTest.java
*
*  Description: The CreditAccountTest class creates a series of tests for the CreditAccount class
*               Tests include:
*                  1. Test getBalance ()
*                  2. Test large deposit ($1000000.00)
*                  3. Add Checking balance and Credit balance
*
*  @author Mark O'Grady
*  @version 1.00.2013.07.25		revised: 2014.09.12
*/

public class CreditAccountTest {
	
	static CreditAccount cr_acct;
	static CheckingAccount ckng_acct;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		cr_acct = new CreditAccount ();
		ckng_acct = new CheckingAccount ();
	}

	@After
	public void tearDown() throws Exception {
		cr_acct = null;
		ckng_acct = null;
	}

	
	@Test
    public void testGetBalance() throws Exception {
        ckng_acct.deposit(100.00);
        cr_acct.setCh(ckng_acct);
        
        Assert.assertTrue("Balance is incorrect", 200 == cr_acct.getBalance());
    }

    @Test
    public void testCanDepositLargeAmount () throws Exception {
        cr_acct.deposit(1000000.00);
        
        Assert.assertEquals("Account balance incorrect", 1000000.00, cr_acct.getBalance(), .01);
    }

    @Test
    public void testBalanceOnCheckingIsAddedToBalanceOnCredit () throws Exception {
        ckng_acct.deposit(100.00);
        cr_acct.setCh(ckng_acct);
        cr_acct.deposit(1000000.00);
        
        Assert.assertEquals("Account balance incorrect", 1000200.00, cr_acct.getBalance(), .01);
    }

}
