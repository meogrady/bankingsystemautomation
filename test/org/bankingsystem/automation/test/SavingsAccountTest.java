package org.bankingsystem.automation.test;

import org.bankingsystem.automation.SavingsAccount;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.DecimalFormat;

/** Copyright (C) 2013 Mark O'Grady. All Rights Reserved.
*
*  Project:     Grey-box Testing with JUnit
*  Program:     SavingAccount.java
*
*  Description: The SavingsAccountTest class creates a series of tests for the SavingsAccount class
*               Tests include:
*                  1. Deposit $100.00
*                  2. Deposit $1000001.00
*                  3. Negative amount
*                  4. Deposit $500,000.00 Twice plus one cent
*                  5. Deposit one cent
*                  6. Fractional Cents
*
*  @author Mark O'Grady
*  @version 1.00.2013.07.25
*/

public class SavingsAccountTest {
	
	static SavingsAccount sa;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println ("Begin Executing SavingsAccount Test...");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println ("End Executing SavingsAccount Test...");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		sa = new SavingsAccount ();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		sa = null;
	}

	/** DEPOSIT TESTS */
	
    @Test
    public void testDeposit100Dollars () throws Exception {
        // Test deposit of $100.00
        boolean result = sa.deposit(100.00);
        Assert.assertTrue("Failed: Cannot Deposit $100.00", result && sa.getSavingsBalance() == 100.00);
    }

    @Test
    public void testDeposit1000001Dollars () throws Exception {
        // Test deposit in excess of $1000000.00
        boolean result = sa.deposit(1000001.00);
        Assert.assertTrue("Failed: Can Deposit $1000001.00", !result && sa.getSavingsBalance() != 1000001.00);
    }

    @Test
    public void testDepositNegativeAmount () throws Exception {
        // Test deposit negative amount
        boolean result = sa.deposit(-1.00);
        Assert.assertTrue("Failed: Can Deposit $-1.00", !result && sa.getSavingsBalance() != -1.00);
    }

    @Test
    public void testDeposit500000DollarsTwice () throws Exception {
        sa.deposit (500000.00);
        Assert.assertTrue("Balance not 50K ", sa.getSavingsBalance() == 500000.00);
        sa.deposit(500000.00);
        Assert.assertTrue("Balance not 100K ", sa.getSavingsBalance() == 1000000.00);
        sa.deposit(0.01);
        Assert.assertTrue("Cannot deposit $100001.00 ", sa.getSavingsBalance() != 1000000.01);
    }

    @Test
    public void testDepositOneCent () throws Exception {
        // Test deposit negative amount
        sa.deposit(0.01);
        Assert.assertTrue("Failed: Can Deposit $-1.00", sa.getSavingsBalance() == 0.01);
    }

    @Test
    public void testDepositFractionalCents () throws Exception {
    	// Deposit fractional cents
        sa.deposit(0.002);
        sa.deposit(0.005);
        Assert.assertTrue("", sa.getSavingsBalance() == roundTwoDecimals(0.002, 0.005));
    }

    /*@Test
    public void testVerifyNumberAccountsCreated () throws Exception {

        SavingsAccount s01 = new SavingsAccount();
        SavingsAccount s02 = new SavingsAccount();
        SavingsAccount s03 = new SavingsAccount();
        SavingsAccount s04 = new SavingsAccount();
        SavingsAccount s05 = null;
        Assert.assertEquals(4, SavingsAccount.getTotalNumberOfSavingsAccounts());
    }*/

    /** WITHDRAWAL TESTS */

    @Test
    public void testWithdraw() throws Exception {
        // Test withdrawal
        sa.deposit(10.00);
        sa.withdraw(6.00);
        Assert.assertTrue("Failed: Cannot make withdrawal", sa.getSavingsBalance() == 4.00);
        Assert.assertTrue("Failed: Balance is $10.00", sa.getSavingsBalance() != 10.00);

    }

    @Test
    public void testWithdrawNegativeBalance() throws Exception {
        // Test withdraw negative balance
        sa.deposit(1.00);
        sa.withdraw(1.01);
        Assert.assertTrue("Failed: Negative Balance", sa.getSavingsBalance() != -0.01);
    }

    @Test
    public void testWithdrawNegativeAmount() throws Exception {
        // Test withdraw negative amounts
        sa.deposit(1.00);
        sa.withdraw(-1.00);
        Assert.assertTrue("Failed: Cannot make withdrawal", sa.getSavingsBalance() != -1.00);
    }

    @Test
    public void testWithdrawFractionalCents() throws Exception {
        // Test withdraw fractional cents
        sa.deposit(0.01);
        sa.withdraw(0.002);
        sa.withdraw(0.005);
        Assert.assertTrue("Failed: Fractional cents", sa.getSavingsBalance() == 0);
    }

    /**
     * Helper method to help out with number formatting and rounding
     * @param d1
     * @param d2
     * @return
     */
    public double roundTwoDecimals (double d1, double d2) {
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(d1 + d2));
    }
}
