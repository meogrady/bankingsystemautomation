package org.bankingsystem.automation.test;

import java.util.List;

import org.bankingsystem.automation.SeleniumTestClassLib;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/** Copyright (C) 2013 Mark O'Grady. All Rights Reserved.
*
*  Project:     Grey-box Testing with JUnit
*  Program:     SavingAccount.java
*
*  Description: The SeleniumWebDriverTest class creates a series of tests to test the the Web
*               Tests include: testBasicSearch ()
*
*  @author Mark O'Grady
*  @version 1.00.2013.07.25   revised: 2014.09.12
*/

public class SeleniumWebDriverTest {
	
	@Test
    public void testBasicSearch () throws Exception {

        SeleniumTestClassLib sellib = new SeleniumTestClassLib();
        WebDriver driver = new FirefoxDriver() ;
        sellib.BrowserSetSearchTerms("Alice", driver);
        sellib.BrowserSetSearchClick(driver);

        sellib.ForceBrowserToWait(20, driver);
        List <WebElement> elements =  driver.findElements (By.partialLinkText("Alice"));
        org.junit.Assert.assertTrue (elements.size() > 5);

        // Close browser
        if (! driver.equals(null)) driver.close();
    } 

}
