package org.bankingsystem.automation.test;

//import com.sun.org.apache.xpath.internal.objects.XRTreeFragSelectWrapper;\
import org.bankingsystem.automation.CheckingAccount;
import org.bankingsystem.automation.LogFileManagerTestClass;
//import org.junit.Assert.*;
import org.junit.*;

//import org.openqa.selenium.support.ui.Wait;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import sun.management.FileSystem;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.Date;
//import java.util.concurrent.TimeUnit;

/** Copyright (C) 2013 Mark O'Grady. All Rights Reserved.
 *
 *  Project:     Grey-box Testing with JUnit
 *  Program:     SavingAccount.java
 *
 *  Description: The CheckingAccountTest class creates a series of tests to test the CheckingAccount class
 *               Tests include: Deposit $100.00
 *
 *  @author Mark O'Grady
 *  @version 1.00.2013.07.25
 */

public class CheckingAccountTest {
    
	private static CheckingAccount ckng_acct;

    @BeforeClass
    public static void beforeClass () {
        System.out.println("Start executing Checking Account test cases...");
    }

    @AfterClass
    public static void afterClass () {
        System.out.println("Ending executing Checking Account test cases");
    }

    @Before
    public void beforeMethod () {
    	ckng_acct = new CheckingAccount ();
    }

    @After
    public void afterMethod () { ckng_acct = null; }

    @Test
    public void testDeposit100Dollars () throws Exception {
    	ckng_acct.deposit (100.00);
        Assert.assertTrue("Cannot deposit $100.00 ", ckng_acct.getBalance() == 100.00);
    }

    @Test
    public void testDeposit100001Dollars () throws Exception {
    	ckng_acct.deposit (100001.00);
        Assert.assertTrue("Cannot deposit $100001.00 ", ckng_acct.getBalance() != 100001.00);
    }

    @Test
    public void testDeposit50000DollarsTwice () throws Exception {
    	ckng_acct.deposit (50000.00);
        Assert.assertTrue("Balance not 50K ", ckng_acct.getBalance() == 50000.00);
        ckng_acct.deposit (50000.00);
        Assert.assertTrue("Balance not 100K ", ckng_acct.getBalance() == 100000.00);
        ckng_acct.deposit (0.01);
        Assert.assertTrue("Cannot deposit $100001.00 ", ckng_acct.getBalance() != 100000.01);
    }

    @Test
    public void testDepositExceedMaxAmount () throws Exception {
        ckng_acct.deposit(1000001.00);
        Assert.assertTrue("Transaction cannot proceed: Deposit exceeds max balance.", ckng_acct.getBalance() != 10000001.00);
    }

    @Test
    public void testDepositOneCent () throws Exception {
        ckng_acct.deposit(0.01);
        Assert.assertTrue("Deposit one cent test failed: .", ckng_acct.getBalance() == 0.01);
    }

    @Test
    public void testDepositNegativeAmount () throws Exception {
        double balanceBeforeDeposit = ckng_acct.getBalance();
        ckng_acct.deposit(-1.00);
        Assert.assertTrue("Negative amt ", ckng_acct.getBalance() == balanceBeforeDeposit);
    }

    @Test
    public void testWithdraw() throws Exception {
    	ckng_acct.deposit(10.00);
    	ckng_acct.withdraw (6.00);
        Assert.assertTrue("", ckng_acct.getBalance() == 4.00);
        Assert.assertTrue("", ckng_acct.getBalance() != 10.00);
    }

    @Test
    public void testWithdrawBelowMinAccountBalance() throws Exception {
    	ckng_acct.deposit(5.00);
    	ckng_acct.withdraw(0.01);
        Assert.assertFalse("", ckng_acct.getBalance() >= ckng_acct.get_MIN_ACCOUNT_BALANCE());
    }

    @Test
    public void testDepositFractionalCents () throws Exception {
        ckng_acct.deposit(0.002); // Deposit 2/10 of one cent
        ckng_acct.deposit(0.005); // Deposit 5/10 of one cent

        Assert.assertTrue("Failed: Fractional cents not deposited", ckng_acct.getBalance() == roundTwoDecimals(0.002, 0.005));
    }

    @SuppressWarnings("unused")
	@Test
    public void testVerifyNumberAccountCreated () throws Exception {
        ckng_acct.resetTotalNumberOfAccounts();
        CheckingAccount ch01 = new CheckingAccount();
        CheckingAccount ch02 = new CheckingAccount();
        CheckingAccount ch03 = new CheckingAccount();
        CheckingAccount ch04 = new CheckingAccount();
        CheckingAccount ch05 = null;
        Assert.assertEquals(4, CheckingAccount.getTotalNumberOfAccount());
    }

    @Test
    public void testCreationDateIsCorrect () throws Exception{
        Assert.assertEquals(String.valueOf(new Date()), String.valueOf(ckng_acct.getAccountCreationDate()));
    }

    @Test
    public void testWithdrwaMoneyReportedWhenNoFileExists () throws Exception{
    	
        File f = new File("/home/meo/dev/junit/bankingsystemautomation/logs/test.txt");
        if (!f.exists()) f.delete();
        ckng_acct.deposit(10);
        
        Assert.assertTrue(f.exists());
    }

    @Test
    public void testWithdrwaMoneyReportedWhenNoFileExistsAndFileContainsInfo () throws Exception{

        File f = new File ("/home/meo/dev/junit/bankingsystemautomation/logs/test.txt"); //("C:\\Documents and Settings\\Mark\\javaapps\\bankingsystemautomation\\logs\\test.txt");
        if (!f.exists()) f.delete();
        ckng_acct.deposit(10);

        FileReader fr = new FileReader("/home/meo/dev/junit/bankingsystemautomation/logs/test.txt");//FileReader("C:\\Documents and Settings\\Mark\\javaapps\\bankingsystemautomation\\logs\\test.txt");
        LineNumberReader lnr = new LineNumberReader(fr);
        int linenumber = 0;

        while (lnr.readLine() != null){
            linenumber++;
        }
        lnr.close();
        Assert.assertTrue("", linenumber > 0);
    }

    @Test
    public void testLogFileManagerCanBePassedToCheckingAccount () throws Exception{

        LogFileManagerTestClass testL = new LogFileManagerTestClass("/home/meo/dev/junit/bankingsystemautomation/logs/test.txt");//"C:\\IdeaProjects\\Bank Account\\logs\\test.txt");

        CheckingAccount ch = new CheckingAccount(testL);
        ch.deposit(10);

        FileReader fr = new FileReader("/home/meo/dev/junit/bankingsystemautomation/logs/test.txt");
        LineNumberReader lnr = new LineNumberReader(fr);
        int linenumber = 0;

        while (lnr.readLine() != null){
            linenumber++;
        }
        lnr.close();
        fr.close();
        Assert.assertTrue("", linenumber > 0);
    }

    @Test
    public void testIfScreenShotCanBeCaptured () throws Exception {
        FileWriter fw = null;
        try {
            fw = new FileWriter("C:\\gjihl\\ghter\\One.txt");
            fw.write("This is message");
        } catch (Exception e) {
            //Process p = Runtime.getRuntime().exec("C:\\Test\\ScreenShotProjFM2.exe C:\\Test\\SSAug201.png");
            System.out.println("Exception occurred");
        } finally {
           // if (! fw.equals(null)) fw.close();
        }
    }

    /**
     *
     * @param d1 left operand
     * @param d2 right operand
     * @return formatted sum of d1 and d2
     */
    public double roundTwoDecimals (double d1, double d2) {
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(d1 + d2));
    }
}

