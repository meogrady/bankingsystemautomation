package org.bankingsystem.automation;

import java.io.FileWriter;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Student
 * Date: 8/13/13
 * Time: 10:30 PM
 * To change this template use File | Settings | File Templates.
 */

public class LogFileManager implements LogFileManagerInterface {

    private String pathToLogFile = "F:\\IdeaProjects\\Bank Account\\logs\\test.txt";

    public LogFileManager () {
    	// Empty
    }

    public LogFileManager (String testPath) {
        pathToLogFile = testPath;
    }

    public  void LFMLogInformation (String info) {
        Date d = new Date();
        try {
            FileWriter fw =  new FileWriter(pathToLogFile, true);
            fw.write(d + " Current balance is " + info + System.getProperties(  ));
            fw.close();
        } catch (Exception e) {
            System.out.println("");
        } finally {
            // Empty
        }
    }
}
