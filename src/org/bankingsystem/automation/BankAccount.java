package org.bankingsystem.automation;

import java.util.Date;

/**
 * Models a bank account...
 * 
 * @author Mark O'Grady
 * @version 2014.06.24 r1
 */
public class BankAccount {
    private double balance;
    private String accountNumber;
    private String clientName;
    private Date accountCreationDate;

    /**
     * Constructs a new bank account with default values.
     */
    public BankAccount () {
        balance = 0.00;
        accountNumber = "Z01";
        clientName = "Nobody";
        accountCreationDate = new Date ();
    }

    /**
     * Constructs a new bank account with initial amount, account number, and
     * client's name.
     * 
     * @param initAmt
     * @param acctNum
     */
    public BankAccount (double initAmt, String acctNum, String cliName) {
        balance = initAmt;
        accountNumber = acctNum;
        clientName = cliName;
        accountCreationDate = new Date ();
    }

    /**
     * Method performs a deposit.
     *
     * @param amount the deposit amount
     */
    public void deposit (double amount) {
        balance += amount;
    }

    /**
     * Method performs a withdrawal after verifying sufficient funds.
     * 
     * @param amount
     */
    public void withdraw (double amount)
            throws InsufficientFundsException {
        //verify amount < balance
        if (amount > this.getBalance()) {
            throw new InsufficientFundsException (amount, this.getBalance());
        } else {
            balance -= amount;
        }
        //verify balance > MIN - amount
    }

    /**
     * Gets balance amount.
     * 
     * @return the current balance on this bank account
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Gets account number.
     * 
     * @return the account number associated with this Bank Account
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Gets the client name on this account.
     *
     * @return the client name associated with this <code>Bank Account</code>
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * Returns the Date this Account was created in [Day] [Month] [Day of Month] [Time] [Time Zone] [Year] format.
     * Example: Wed Jul 24 13:26:59 PDT 2013
     *
     * @return the date <code>Bank Account</code> was created.
     */
    public Date getAccountCreationDate() {
        return accountCreationDate;
    }

    /**
     *  Returns a string representation of a BankAccount object.
     */
    public void toBAString () {
        System.out.println();
        System.out.println("Account number: " + this.getAccountNumber() +
                "\nBalance is: " + this.getBalance() +
                "\nClient name: " + this.getClientName() +
                "\nToday is: " + this.getAccountCreationDate ());
    }
}
