package org.bankingsystem.automation;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Student
 * Date: 7/30/13
 * Time: 7:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class RandomAccountNumberGenerator {

    public static String GenerateRandomNumber () {
        Random rand = new Random();
        return Double.toString(rand.nextDouble());
    }

    public static boolean GenerateRandomBoolean () {
        Random rand = new Random(100);
        return rand.nextBoolean();
    }
}
