package org.bankingsystem.automation;

/** Copyright (c) 2014 Mark O'Grady. All Rights Reserved.
 * 
 *  Project: 		White-box testing with JUnit
 *  Program: 		Truck.java
 *  
 *  Description:	Provides SUV plus Vehicle attributes.
 *  
 *  Differences between abstract classes and interfaces (needs cleanup)
 *		1. not a class, but type
 * 		2. implementation of methods is prohibited
 * 		3. classes can implement multiple interfaces
 * 		4. methods declared in interface must be implemented in class
 * 		5. all declarations must be public
 * 		6. only declarations and variable and constants
 * 		7. no static objects
 * 
 *  @author   Mark O'Grady
 *  @version  2014.09.12	1.00
 *
 */

public class Truck extends Vehicle {
    @Override
    public void talk () {
        System.out.println("I am a truck");
    }

    /*@Override
    public int getNumberOfWheels() {
        //To change body of implemented methods use File | Settings | File Templates.
        return 4;
    }

    @Override
    public void setNumberOfWheels(int wheels) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isSteeringWheelPresent() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }*/
}
