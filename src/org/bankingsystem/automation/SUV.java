package org.bankingsystem.automation;

/** Copyright (c) 2014 Mark O'Grady. All Rights Reserved.
 * 
 *  Project: White-box testing with JUnit
 *  Program: SUV.java
 *  
 *  Description:    Provides SUV plus Vehicle attributes.
 * 
 *  @author   Mark O'Grady
 *  @version  2014.09.12	1.00
 *
 */

public class SUV extends Vehicle {

	@Override
	public void talk () {
		System.out.println ("I am an SUV");
	}
}
