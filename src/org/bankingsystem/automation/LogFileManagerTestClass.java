package org.bankingsystem.automation;

import java.io.FileWriter;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Student
 * Date: 8/13/13
 * Time: 10:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class LogFileManagerTestClass implements LogFileManagerInterface{

    private String pathToLogFile = "C:\\IdeaProjects\\Bank Account\\logs\\test.txt";

    public LogFileManagerTestClass () {

    }

    public LogFileManagerTestClass (String testPath) {
        pathToLogFile = testPath;
    }

    public  void LFMLogInformation (String info) {
        Date d = new Date();
        try {
            FileWriter fw =  new FileWriter(pathToLogFile, true);
            fw.write(d + " Current balance is " + info + System.getProperties(  ));
            fw.close();
        } catch (Exception e) {
            System.out.println("");
        } finally {
            // Empty
        }
    }
}

