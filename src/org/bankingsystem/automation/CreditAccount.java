package org.bankingsystem.automation;

/**
 * Created with IntelliJ IDEA.
 * User: Student
 * Date: 7/30/13
 * Time: 8:15 PM
 * To change this template use File | Settings | File Templates.
 */


public class CreditAccount extends AbstractAccount {

    CheckingAccount ch = null;

    public void deposit (double amount) {
        this.setBalance(this.balance + amount);
    }

    public double withdraw (double amount) {
        return 0;
    }

    public double getBalance () {
        if (ch != null) {
            return 2 * ch.getBalance() + this.balance;
        } else {
            return this.balance;
        }
    }

    public CheckingAccount getCh() {
        return ch;
    }

    public void setCh(CheckingAccount ch) {
        this.ch = ch;
    }
}
