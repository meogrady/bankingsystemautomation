package org.bankingsystem.automation;

/**
 * Created with IntelliJ IDEA. 
 * User: Mark Date: 7/24/13 Time: 2:48 PM To change
 * this template use File | Settings | File Templates.
 */
public class InsufficientFundsException extends Exception {
	static final long serialVersionUID = 42L;
	
	private double amount;
	private double balance;

	public InsufficientFundsException(double amount, double balance) {
		this.amount = amount;
		this.balance = balance;
		System.out.println("Insufficient Funds..." + " Requested amount of "
				+ "$" + this.getAmount() + " exceeds available balance of "
				+ "$" + this.getBalance());
	}

	public double getAmount() {
		return amount;
	}

	public double getBalance() {
		return balance;
	}

	public double getDifference() {
		return balance - amount;
	}
}
