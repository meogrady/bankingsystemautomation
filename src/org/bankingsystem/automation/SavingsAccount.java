package org.bankingsystem.automation;

import java.text.DecimalFormat;
import java.util.Date;

/** Copyright (C) 2013 Mark O'Grady. All Rights Reserved.
*
*  Project:     Grey-box Testing with JUnit
*  Program:     SavingAccount.java
*
*  Description: The SavingsAccount class creates a savings account with appropriate accessors/mutators.
*               A <code>SavingsAccount</code> has no minimum balance, but does have a maximum balance
*               of $1000000.00 (one million) USD. The interest rate on a savings account is 0.02 (2%)
*               compound . The date of account creation is recorded using system time.
*
*  @author Mark O'Grady
*  @version 2013.07.25 1.0
*/

public class SavingsAccount {
	private final double INTEREST_RATE = 0.02;
    private final double MAXIMUM_BALANCE = 1000000.00;

    private Date accountCreationDate = null;
    private double savingsBalance;
    private String savingsAccountNumber;

    private static int totalNumberOfSavingsAccounts = 0;

    /**
     * Default constructor builds a SavingAccount object with default values.
     */
    public SavingsAccount() {
        accountCreationDate = new Date();
        savingsAccountNumber = "Z01";
        savingsBalance = 0.00;
        totalNumberOfSavingsAccounts++;
    }

    /**
     * Constructs a SavingsAccount object with an auto-gen date, a client name, account number
     * and initial balance.
     *
     * @param acctNum the account number associated with this client
     * @param initialBalance the initial balance on account as of creation date.
     */
    public SavingsAccount(String acctNum, double initialBalance) {
        accountCreationDate = new Date();
        savingsAccountNumber = acctNum;
        savingsBalance = initialBalance;
        totalNumberOfSavingsAccounts++;
    }

    /**
     * Gets the account number on this savings account.
     * 
     * @return the savings account number.
     */
    public String getSavingsAccountNumber() {
        return savingsAccountNumber;
    }

    /**
     * Gets the balance on this savings account.
     * 
     * @return the savings account balance.
     */
    public double getSavingsBalance() {
        return roundToDecimal (savingsBalance);
    }

    /**
     * Sets the savings balance to the value of <CODE>savingsBalance</CODE>.
     * 
     * @param savingsBalance
     */
    private void setSavingsBalance(double savingsBalance) {
        this.savingsBalance = savingsBalance;
    }

    /**
     * Gets account creations date as set in constructor.
     * 
     * @return the account creation date on this account.
     */
    public Date getAccountCreationDate() {
        return accountCreationDate;
    }

    /**
     * Gets the total number of savings accounts.
     * 
     * @return the total number of savings accounts.
     */
    public static int getTotalNumberOfSavingsAccounts () {
        return totalNumberOfSavingsAccounts;
    }

    /**
     * Method to allow client to deposit funds into their account provided
     * the sum of the current balance and the deposit amount do not exceed MAXIMUM_BALANCE.
     *
     * @param amount the amount to deposit
     * @return Returns <code>true</code> if transaction succeeds;<code>false</code> otherwise.
     */
    public boolean deposit (double amount) {
        // Amount is negative
        if (amount < 0)
            return false;
        // Attempting to deposit in excess of maximum allowable balance
        if (this.getSavingsBalance() + amount > MAXIMUM_BALANCE)
            return false;

        this.setSavingsBalance(this.savingsBalance + amount);
        return true;
    }

    /**
     * Method to allow client to withdraw funds from their account provided
     * enough available funds exist in the account.
     *
     * @param amount the amount to withdraw
     * @return Returns <code>true</code> if transaction succeeds;<code>false</code> otherwise.
     */
    public boolean withdraw (double amount) {
        if (amount > this.getSavingsBalance())
            return false;

        this.setSavingsBalance(this.getSavingsBalance() - amount);
        return true;
    }

    /**
     * The calculateInterestCompoundMonthly method calculates interest on a savings account
     * compound monthly according to the formula; P (1 + r/12)^12
     *  where:
     *      P is current balance
     *      r is the interest rate.
     *
     * @return the current balance with calculated interest for this month.
     */
    public double calculatedInterestCompoundMonthly () {
        return this.getSavingsBalance() * Math.pow((1 + INTEREST_RATE/12), 12);
    }

    /**
     * Calculates the amount of daily interest on current balance.
     *
     * @return amount of daily interest on this balance.
     */
    public double calculatedDailyInterest () {
        return this.getSavingsBalance() * INTEREST_RATE/365;
    }

    /**
     * Formats value in accordance with the half-even rounding algorithm.
     * 
     * @param amt
     * @return the formatted value.
     */
    public double roundToDecimal (double amt) {
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(amt));
    }
}
