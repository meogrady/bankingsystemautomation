package org.bankingsystem.automation;

/** Copyright (c) 2014 Mark O'Grady. All Rights Reserved.
 * 
 *  Project: White-box testing with JUnit
 *  Program: ITest.java
 *  
 *  Desc:    Interface provide method for testing...
 * 
 *  @author   Mark O'Grady
 *  @version  2014.09.12	1.00
 *
 */

public interface ITest {

	public void foo ();
}
