package org.bankingsystem.automation;

/** Copyright (C) 2013 Mark O'Grady. All Rights Reserved.
*
*  Project:     Grey-box Testing with JUnit
*  Program:     AccountType.java
*
*  Description: The <code>AccountType</code> class creates an enumerated class...
*
*  @author Mark O'Grady
*  @version 1.00.2013.07.27
*/

public enum AccountType {
	CHECKING, SAVING, CREDIT, LOAN, UNKNOWN
}
