package org.bankingsystem.automation;

/** Copyright (C) 2013 Mark O'Grady. All Rights Reserved.
 *
 *  Project:     Grey-box Testing with JUnit
 *  Program:     AbstractAccount.java
 *
 *  Description: The AbstractAccount class creates a savings account with appropriate accessors/mutators.
 *               A <code>AbstractAccount</code> has no minimum balance, but does have a maximum balance
 *               of $1000000.00 (one million) USD. The interest rate on a savings account is 0.02 (2%)
 *               compound . The date of account creation is recorded using system time.
 *
 *               abstract features:
 *               1. one or more methods not implemented and are abstract type
 *               2. can contain non-abstract methods
 *               3. cannot create instance of abstract class
 *               4. abstract method can be not defined in derived class
 *               5. can have constructor
 *
 *  @author Mark O'Grady
 *  @version 1.00.2013.07.25
 */

import java.text.DecimalFormat;
import java.util.Date;

public abstract class AbstractAccount {

    protected double balance;
    protected Date accountCreationDate;
    private String accountNumber;
    private AccountType type;

    private static int totalNumberOfCheckingAccounts = 0;

    public AbstractAccount () {
        totalNumberOfCheckingAccounts++;
    }

    public double getBalance() { // Changed from protected to public
        return roundToDecimals(balance);
    }

    protected void setBalance(double balance) {
        this.balance = balance;
    }

    public static int getTotalNumberOfAccount() {
        return totalNumberOfCheckingAccounts;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

	private void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void displayAccountBalance () {
        System.out.println("Account balance " + this.getBalance());
    }

    public Date getAccountCreationDate() {
        return accountCreationDate;
    }

    private void setAccountCreationDate(Date accountCreationDate) {
        this.accountCreationDate = accountCreationDate;
    }

    public double roundToDecimals (double amt) {
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(amt));
    }

    public void resetTotalNumberOfAccounts () {
        totalNumberOfCheckingAccounts = 0;
    }
    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    /**
     * Make a deposit to this account.
     * @param amt
     */
    public abstract void deposit (double amt);

    /**
     *
     * @param amt
     * @return
     */
    public abstract double withdraw (double amt);

    public void display () {
        System.out.println("");
    }
}

