package org.bankingsystem.automation;

/** Copyright (c) 2014 Mark O'Grady. All Rights Reserved.
 * 
 *  Project: White-box testing with JUnit
 *  Program: Vehicle.java
 *  
 *  Description:    Provides basic vehicle attributes.
 * 
 *  @author   Mark O'Grady
 *  @version  2014.09.12	1.00
 *
 */

public class Vehicle implements ITest, IVehicle {

	private int _numberOfWheels = 4;
	
	@Override
	public void talk() {
		System.out.println ("I am a generic vehicle");
	}

	@Override
	public int getNumberOfWheels() {
		return _numberOfWheels;
	}

	@Override
	public void setNumberOfWheels(int n) {
		_numberOfWheels = n;
	}

	@Override
	public boolean isSteeringWheelPresent() {
		return false;
	}

	@Override
	public void foo() {
		System.out.println ("...bar");
	}

}
