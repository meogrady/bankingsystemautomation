package org.bankingsystem.automation;

import java.util.Date;
import java.util.ArrayList;

/** Copyright (c) 2014 Mark O'Grady. All Rights Reserved.
 * 
 *  Project: White-box testing with JUnit
 *  Program: Person.java
 *  
 *  Description:    Provides Person attributes.
 * 
 *  @author   Mark O'Grady
 *  @version  2014.09.12	1.00
 *
 */

public class Person {
	
	private String firstName;
    private String lastName;
    private Date dob;
    private String snn;

    private ArrayList <AbstractAccount> accounts = new ArrayList <AbstractAccount>();

    /**
     * Use singleton pattern
     */
    public Person () {/* Empty */}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDOB() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getSnn() {
        return snn;
    }

    public void setSnn(String snn) {
        this.snn = snn;
    }

    public void printSummary () {
        //acct type
        //balance
    }

    /**
     *
     * @param ca
     * @return
     */
    /*public boolean addAccountToTheList (CreditAccount ca) {
        try {
            this.accounts.add(ca);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    *//**
     *
     * @param ch
     * @return
     *//*
    public boolean addAccountToTheList (CheckingAccount ch) {
        try {
            this.accounts.add(ch);
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/

    public boolean addAccountToTheList (AbstractAccount abs) {
        try {
            this.accounts.add(abs);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public int getNumberOfAccounts () {
        return this.accounts.size();
    }

    public AccountType getAccountByPosition (int index) {
        if (index >= this.accounts.size()) {
            return AccountType.UNKNOWN;
        }
        if (this.accounts.get(index) instanceof  CreditAccount) {
            return AccountType.CREDIT;
            //return ((CheckingAccount) this.accounts.get(index)).type;
        } else if (this.accounts.get(index) instanceof CheckingAccount) {
            return AccountType.CHECKING;
        } else {
            return AccountType.SAVING;
        }
    }
}
