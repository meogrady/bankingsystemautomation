package org.bankingsystem.automation;

/** Copyright (c) 2014 Mark O'Grady. All Rights Reserved.
 * 
 *  Project: White-box testing with JUnit
 *  Program: IVehicle.java
 *  
 *  Desc:    Interface provide method for testing...
 * 
 *  @author   Mark O'Grady
 *  @version  2014.09.12	1.00
 *
 */

public interface IVehicle {
	/**
     * Make sure the classes use variable numberOfWheels
     * to check actual number of wheels
     */

    /**
     *
     */
    public void talk ();

    /**
     *
     * @return
     */
    public int getNumberOfWheels ();

    /**
     *
     * @param n
     */
    public void setNumberOfWheels (int n);

    /**
     *
     * @return
     */
    public boolean isSteeringWheelPresent ();

    /**
     *
     */
    public int numberOfWheels = 4;

}
