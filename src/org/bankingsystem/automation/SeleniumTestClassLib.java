package org.bankingsystem.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Student
 * Date: 8/20/13
 * Time: 9:46 PM
 * To change this template use File | Settings | File Templates.
 */

public class SeleniumTestClassLib {

    public boolean BrowserSetSearchTerms (String searchTerms , WebDriver driver) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 20);
            driver.get ("http://www.google.com");

            WebElement element1 = driver.findElement (By.id("gbqfq")); // not maintainable b/c google may change from gbqfg to some other sting

            if (element1.equals(null)){
                element1 = driver.findElement(By.name("g"));
            }

            if (element1.equals(null)){
                element1 = driver.findElement(By.tagName("text"));
            }

            element1.sendKeys (searchTerms);
            return true;

        }   catch (Exception e) {
            return false;
        }
    }

    public boolean BrowserSetSearchClick (WebDriver driver) {
        try {
            WebElement elementLink = driver.findElement (By.name("btnG")); // not maintainable b/c google may change from gbqfg to some other sting
            elementLink.click();

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return true;
        }   catch (Exception e) {
            return false;
        }
    }

    public  void ForceBrowserToWait (int timeout, WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

}

