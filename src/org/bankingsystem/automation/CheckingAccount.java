package org.bankingsystem.automation;

import java.text.DecimalFormat;
import java.util.Date;

/** Copyright (C) 2013 Mark O'Grady. All Rights Reserved.
 *
 *  Project:     Grey-box Testing with JUnit
 *  Program:     SavingAccount.java
 *
 *  Description: The CheckingAccount class creates a checking account with methods to get/set balance, etc.
 *               A savings account has a minimum balance $5.00 USD, and a maximum balance
 *               of $1000000.00 (one million) USD. There is no fee for using the account.
 *               The date of account creation is recorded using system time.
 *
 *  @author Mark O'Grady
 *  @version 1.00.2013.07.24
 */
public class CheckingAccount extends AbstractAccount {

    private final double MAX_ACCOUNT_BALANCE = 100000;
    private final double MIN_ACCOUNT_BALANCE = 5;
    LogFileManagerInterface lfm;

    private Date accountCreationDate;
    private String accountNumber;

    private static int totalNumberOfCheckingAccounts = 0;

    public CheckingAccount () {
        totalNumberOfCheckingAccounts++;
        accountCreationDate = new Date();
        accountNumber = (String) RandomAccountNumberGenerator.GenerateRandomNumber();
    }

    public CheckingAccount (LogFileManagerInterface lfm) {
        this ();
        this.lfm = lfm;
    }

    public static int getTotalNumberOfAccount() {
        return totalNumberOfCheckingAccounts;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    @SuppressWarnings("unused")
	private void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void displayAccountBalance () {
        System.out.println("Account balance " + this.getBalance());
    }

    public Date getAccountCreationDate() {
        return accountCreationDate;
    }

    @SuppressWarnings("unused")
	private void setAccountCreationDate(Date accountCreationDate) {
        this.accountCreationDate = accountCreationDate;
    }

    public double roundToDecimals (double amt) {
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(amt));
    }

    public double get_MIN_ACCOUNT_BALANCE () {
        return MIN_ACCOUNT_BALANCE;
    }

    public void resetTotalNumberOfAccounts () {
        totalNumberOfCheckingAccounts = 0;
    }

    /**
     * Make a deposit to this account.
     * @param amt
     */
    public void deposit (double amt) {
        if (amt < 0) {
            System.out.println("Cannot proceed with transaction: Amount must be non-negative");
            return;
        }

        if (this.getBalance() + amt > MAX_ACCOUNT_BALANCE) {
            System.out.println("Cannot proceed with transaction: Max Balance reached");
            return;
        }

        this.setBalance (this.balance + amt);
        this.logInformationToFile(Double.toString(this.getBalance()));

    }

    /**
     *
     * @param amt
     * @return
     */
    public double withdraw (double amt) {
        //write info into a file

        // homework: how to access bank server not accessible
        if (amt <= 0) {
            System.out.println();
            return this.getBalance();
        }
        if (this.getBalance() < amt ){
            System.out.println();
            return this.getBalance();
        }
        this.setBalance (this.getBalance() - amt);
        this.logInformationToFile(Double.toString(this.getBalance()));
        return getBalance();
    }

    private void logInformationToFile (String info) {
       LogFileManager lfm = new LogFileManager ();
        lfm.LFMLogInformation(info);
    }

}
